import {
  FETCH_BOARDS,
  FETCH_LISTS,
  FETCH_CARDS,
  FETCH_CHECKLIST,
  POST_CARD,
  POST_CHECKLIST,
  DELETE_CHECKLIST
} from "./types";
import { apiKEY, tokenKEY } from "../url";

export const postLists = list => dispatch => {
  // console.log(list);
  fetch(
    `https://api.trello.com/1/lists?name=${list.listName}&pos=bottom&idBoard=${list.boardId}&key=${apiKEY}&token=${tokenKEY}`,
    {
      method: "POST"
    }
  )
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(err => console.log(err));
};
export const fetchLists = id => dispatch => {
  fetch(
    `https://api.trello.com/1/boards/${id}/lists?key=${apiKEY}&token=${tokenKEY}`
  )
    .then(response => response.json())
    .then(lists => {
      dispatch({
        type: FETCH_LISTS,
        listData: lists
      });
    })
    .catch(err => console.log(err));
};
export const fetchBoards = () => dispatch => {
  fetch(
    "https://api.trello.com/1/members/mohdzafar1/boards?filter=all&fields=all&lists=none&memberships=none&organization=false&organization_fields=name%2CdisplayName&key=b8ca228e1383798870e2f5f79c57417c&token=53e81a8e1c32094e4b7af14e10a2927e3e2e1caa37d896d66bd433ec6fa86e2f"
  )
    .then(response => response.json())
    .then(boards =>
      dispatch({
        type: FETCH_BOARDS,
        boardsData: boards
      })
    )
    .catch(err => console.log(err));
};

export const fetchCards = id => dispatch => {
  fetch(
    `https://api.trello.com/1/lists/${id}/cards?key=${apiKEY}&token=${tokenKEY}`
  )
    .then(response => response.json())
    .then(cards =>
      dispatch({
        type: FETCH_CARDS,
        cardsData: cards,
        listId: cards[0].idList
      })
    )
    .catch(err => console.log(err));
};

export const postCard = (listId, name) => dispatch => {
  fetch(
    `https://api.trello.com/1/cards?name=${name}&idList=${listId}&keepFromSource=all&key=${apiKEY}&token=${tokenKEY}`,
    {
      method: "POST"
    }
  )
    .then(response => response.json())
    .then(card =>
      dispatch({
        type: POST_CARD,
        card: card,
        listId: card.idList
      })
    )
    .catch(err => console.log(err));
};

export const deleteCard = id => dispatch => {
  console.log(id);
  fetch(
    `https://api.trello.com/1/cards/${id}?key=${apiKEY}&token=${tokenKEY}`,
    {
      method: "DELETE"
    }
  )
    .then(response => response.json())
    .then(card => console.log(card))

    .catch(err => console.log(err));
};

export const fetchCheckList = id => dispatch => {
  fetch(
    `https://api.trello.com/1/cards/${id}/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=${apiKEY}&token=${tokenKEY}`
  )
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: FETCH_CHECKLIST,
        checklistData: data
      })
    )
    .catch(err => console.log(err));
};

export const postCheckList = (name, id) => dispatch => {
  fetch(
    `https://api.trello.com/1/checklists?idCard=${id}&name=${name}&key=${apiKEY}&token=${tokenKEY}`,
    {
      method: "POST"
    }
  )
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: POST_CHECKLIST,
        checklist: data
      })
    )

    .catch(err => console.log(err));
};

export const deleteCheckList = id => dispatch => {
  fetch(
    `https://api.trello.com/1/checklists/${id}?key=${apiKEY}&token=${tokenKEY}`,
    {
      method: "DELETE"
    }
  )
    .then(response => response.json())
    .then(data => {})
    .catch(err => console.log(err));
};

export const postCheckListItem = (id, name) => dispatch => {
  fetch(
    `https://api.trello.com/1/checklists/${id}/checkItems?name=${name}&pos=bottom&checked=false&key=${apiKEY}&token=${tokenKEY}`,
    {
      method: "POST"
    }
  )
    .then(response => response.json())
    .then(data => {})
    .catch(err => console.log(err));
};

export const deleteCheckListItem = (checklistId, id) => dispatch => {
  fetch(
    `https://api.trello.com/1/checklists/${checklistId}/checkItems/${id}?key=${apiKEY}&token=${tokenKEY}`,
    {
      method: "DELETE"
    }
  )
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(err => console.log(err));
};
