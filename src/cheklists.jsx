import React, { Component } from "react";
import { apiKEY, tokenKEY } from "./url";
import "./App.css";
import Checklist from "./checklist";
import { connect } from "react-redux";
import {
  fetchCheckList,
  deleteCheckList,
  postCheckList,
  postCheckListItem
} from "./actions/fetchaction";

class Checklists extends Component {
  state = { isLoading: false, checklistname: "" };
  componentDidMount() {
    this.props.fetchCheckList(this.props.id);
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.checklistData !== this.props.checklists) {
      this.props.fetchCheckList(this.props.id);
    }
  }

  addChecklistItem = (id, name) => {
    this.props.postCheckListItem(id, name);
  };
  render() {
    return (
      <div>
        <div className="model-checklist-button">
          <input
            type="text"
            className="checklist-input"
            onChange={this.handleChange}
            value={this.state.checklistname}
          />
          <button onClick={this.addChecklist} className="add-checklist-button">
            Add Checklist
          </button>
        </div>
        <div>
          {this.props.checklists.map(c => (
            <Checklist
              key={c.id}
              name={c.name}
              data={c}
              cardId={this.props.id}
              onDeleteChecklist={this.deleteChecklist}
              addChecklistItem={this.addChecklistItem}
            />
          ))}
        </div>
      </div>
    );
  }
  handleChange = event => {
    this.setState({
      checklistname: event.target.value
    });
  };
  addChecklist = () => {
    this.props.postCheckList(this.state.checklistname, this.props.id);
  };
  deleteChecklist = id => {
    this.props.deleteCheckList(id);
  };
}

const mapStateToProps = state => ({
  checklists: state.boards.checklistData
});

export default connect(mapStateToProps, {
  fetchCheckList,
  deleteCheckList,
  postCheckList,
  postCheckListItem
})(Checklists);
