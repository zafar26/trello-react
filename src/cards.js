import React, { Component } from "react";
// import { apiKEY, tokenKEY } from "./url";
// import { ListGroup } from "react-bootstrap";
import ModalRender from "./modal2";

class Card extends Component {
  render() {
    return (
      <div className="card">
        <ModalRender name={this.props.name} id={this.props.data.id} />
        <button
          id={this.props.data.id}
          onClick={() => {
            this.props.onDelete(this.props.data.id);
          }}
          className="delete-card"
        >
          X
        </button>
      </div>
    );
  }
}

export default Card;
