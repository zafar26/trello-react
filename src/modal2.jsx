import React, { Component } from "react";
import "./App.css";
import OpenModal from "./openModal";

class ModalRender extends Component {
  state = { open: false };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
  render() {
    const { open } = this.state;
    if (open) {
      return (
        <div className="card-modal-button">
          <button onClick={this.onOpenModal}>{this.props.name}</button>
          <OpenModal
            name={this.props.name}
            id={this.props.id}
            open={open}
            onClose={this.onCloseModal}
          />
        </div>
      );
    }
    return (
      <div className="card-modal-button">
        <button onClick={this.onOpenModal}>{this.props.name}</button>
      </div>
    );
  }
}

export default ModalRender;
