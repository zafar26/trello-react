import React, { Component } from "react";
import BoardCard from "./boardcard";
import { connect } from "react-redux";
import { fetchBoards } from "./actions/fetchaction";

class Board extends Component {
  componentWillMount() {
    this.props.fetchBoards();
  }
  render() {
    return (
      <div className="home-board">
        {this.props.boards.map(c => (
          <BoardCard data={c} />
        ))}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  boards: state.boards.data
});

export default connect(mapStateToProps, { fetchBoards })(Board);
