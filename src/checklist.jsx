import React, { Component } from "react";
import ChecklistItem from "./checklistitem";

class Checklist extends Component {
  state = {
    checkitems: this.props.data.checkItems,
    checklistitemName: ""
  };
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.data.checkItems !== this.state.checkitems) {
      this.setState({
        checkitems: this.props.data.checkItems
      });
    }
  }

  render() {
    return (
      <div className="checklists">
        <div className="checklist-name">
          {this.props.name}
          <button
            onClick={() => {
              this.props.onDeleteChecklist(this.props.data.id);
            }}
          >
            Delete
          </button>
        </div>
        <hr></hr>
        <div className="input-checklist-item">
          <input
            onChange={this.handleChange}
            value={this.state.checklistitemName}
            type="text"
          />
          <button
            onClick={() => {
              this.props.addChecklistItem(
                this.props.data.id,
                this.state.checklistitemName
              );
            }}
            className="add-checklist-item-button"
          >
            Add Item
          </button>
        </div>
        <div className="checklist-items">
          {this.state.checkitems.map(c => (
            <ChecklistItem
              key={c.id}
              name={c.name}
              state={c.state}
              id={c.id}
              checklistId={this.props.data.id}
              cardId={this.props.cardId}
            />
          ))}
        </div>
      </div>
    );
  }
  handleChange = event => {
    this.setState({
      checklistitemName: event.target.value
    });
  };
}

export default Checklist;
