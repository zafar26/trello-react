import React from "react";
// import { apiKEY, tokenKEY } from "./url";
import "./App.css";
import Card from "./cards";
import { connect } from "react-redux";
import { fetchCards, postCard, deleteCard } from "./actions/fetchaction";

class Lists extends React.Component {
  state = {
    input: ""
  };

  componentDidMount() {
    this.props.fetchCards(this.props.listdata.id);
  }
  // getCards() {
  //   fetch(
  //     `https://api.trello.com/1/lists/${this.state.data.id}/cards?key=${apiKEY}&token=${tokenKEY}`
  //   )
  //     .then(response => response.json())
  //     .then(data => this.setState({ cardData: data }))
  //     .catch(err => console.log(err));
  // }

  render() {
    var carData = [];
    Object.entries(this.props.cards).map(c => {
      var keys = Object.keys(c[1]);
      var values = Object.values(c[1]);
      if (keys[0] === `card-${this.props.listdata.id}`) {
        carData = values[0];
      }
    });
    return (
      <div className="board">
        <h3>{this.props.listdata.name}</h3>
        <div id="all-cards">
          {carData.map(c => (
            <Card
              key={c.id}
              name={c.name}
              data={c}
              onDelete={this.deleteCard}
            />
          ))}
        </div>
        <div className="add-card">
          <textarea
            name="card-title"
            id="card-title"
            cols="22"
            rows="3"
            placeholder="Enter a title For this Card"
            onChange={this.handleChange}
            value={this.state.input}
          ></textarea>
          <i className="fas fa-pencil-alt prefix"></i>
          <div id="buttons">
            <button id="add-card" onClick={this.addCard}>
              Add Card
            </button>
            <button id="close">X</button>
          </div>
        </div>
      </div>
    );
  }

  handleChange = event => {
    this.setState({
      input: event.target.value
    });
  };

  addCard = () => {
    this.props.postCard(this.props.listdata.id, this.state.input);
  };
  //   fetch(
  //     `https://api.trello.com/1/cards?name=${this.state.input}&idList=${this.state.data.id}&keepFromSource=all&key=${apiKEY}&token=${tokenKEY}`,
  //     {
  //       method: "POST"
  //     }
  //   )
  //     .then(response => response.json())
  //     .then(data => {
  //       this.setState({ cardData: this.state.cardData.concat([data]) });
  //     })
  //     .catch(err => console.log(err));
  // };
  deleteCard = id => {
    this.props.deleteCard(id);
  };
  //     fetch(
  //       `https://api.trello.com/1/cards/${id}?key=${apiKEY}&token=${tokenKEY}`,
  //       {
  //         method: "DELETE"
  //       }
  //     )
  //       .then(response => response.json())
  //       .then(data => {
  //         var cardData = this.state.cardData.filter(c => c.id !== id);
  //         this.setState({ cardData });
  //       })
  //       .catch(err => console.log(err));
  //   };
  // }

  // export default Lists;
}
const mapStateToProps = state => ({
  cards: state.boards.cardsData
});

export default connect(mapStateToProps, { fetchCards, postCard, deleteCard })(
  Lists
);
