import React, { Component } from "react";
import { apiKEY, tokenKEY } from "./url";
import { deleteCheckListItem } from "./actions/fetchaction";
import { connect } from "react-redux";

class ChecklistItem extends Component {
  state = {
    checked: false
  };
  render() {
    var state = false;
    var style = "uncheckeditem-input";
    if (this.props.state === "complete") {
      state = true;
      style = "checkitem-input";
    }
    return (
      <div className="checkbox-and-label">
        <input
          id={this.props.id}
          type="checkbox"
          onClick={this.handleChecked}
          checked={state}
        />
        <div className={style}>{this.props.name}</div>
        <button
          onClick={this.deleteChecklistItem}
          className="delete-checklist-button"
        >
          X
        </button>
      </div>
    );
  }
  deleteChecklistItem = () => {
    this.props.deleteCheckListItem(this.props.checklistId, this.props.id);
  };
  handleChecked = event => {
    const check = event.target.checked;
    fetch(
      `https://api.trello.com/1/cards/${this.props.cardId}/checkItem/${event.target.id}?state=${check}&key=${apiKEY}&token=${tokenKEY}`,
      {
        method: "PUT"
      }
    )
      .then(response => response.json())
      .then(data => {
        if (data.state === "complete") {
          this.setState({ checked: true });
        } else {
          this.setState({ checked: false });
        }
      })
      .catch(err => console.log(err));
  };
}

// export default ChecklistItem;

const mapStateToProps = state => ({
  checklists: state.boards.checklistData
});

export default connect(mapStateToProps, { deleteCheckListItem })(ChecklistItem);
