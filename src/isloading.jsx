import React, { Component } from "react";
import ReactLoading from "react-loading";

class Loading extends Component {
  state = { state: this.props.state };
  render() {
    if (this.state === true) {
      return (
        <div>
          <ReactLoading
            type={"spinningBubbles"}
            color={"#009688"}
            height={67}
            width={75}
          />
        </div>
      );
    }
  }
}

export default Loading;
