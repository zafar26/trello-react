import React, { Component } from "react";
import Modal from "react-responsive-modal";
import Checklists from "./cheklists";

class OpenModal extends Component {
  state = {};
  render() {
    return (
      <Modal open={this.props.open} onClose={this.props.onClose}>
        <div className="modal-box">
          <h2>{this.props.name}</h2>
          <Checklists name={this.props.name} id={this.props.id} />
        </div>
      </Modal>
    );
  }
}

export default OpenModal;
