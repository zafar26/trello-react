import {
  FETCH_BOARDS,
  FETCH_LISTS,
  FETCH_CARDS,
  FETCH_CHECKLIST,
  POST_LISTS,
  POST_CARD,
  POST_CHECKLIST,
  DELETE_CHECKLIST
} from "../actions/types";

const initialSatate = {
  data: [],
  listData: [],
  cardsData: [],
  list: {},
  cards: [],
  card: [],
  checklistData: []
};
export default function(state = initialSatate, action) {
  switch (action.type) {
    case FETCH_BOARDS:
      return {
        ...state,
        data: action.boardsData
      };
    case FETCH_LISTS:
      return {
        ...state,
        listData: action.listData
      };
    case FETCH_CARDS:
      return {
        ...state,
        cardsData: state.cardsData.concat({
          [`card-${action.listId}`]: action.cardsData
        })
      };
    case FETCH_CHECKLIST:
      return {
        ...state,
        checklistData: action.checklistData
      };
    case POST_LISTS:
      return {
        ...state
        // list: action.list
      };
    case POST_CARD:
      return {
        ...state
        // cardsData: state.cardsData.concat({
        //   [`card-${action.listId}`]: action.card
        // })
      };
    case POST_CHECKLIST:
      return {
        ...state
        // checklist: action.checklist
      };
    case DELETE_CHECKLIST:
      return {
        ...state
        // checklist: action.checklist
      };

    default:
      return state;
  }
}
