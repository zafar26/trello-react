import { combineReducers } from "redux";
import fetchingreducer from "./fetchingboards";

export default combineReducers({
  boards: fetchingreducer
});
