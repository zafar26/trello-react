import React, { Component } from "react";
import Lists from "./lists";
import { connect } from "react-redux";
import { fetchLists, postLists } from "./actions/fetchaction";

class TotalLists extends Component {
  state = {
    id: this.props.match.params.id,
    // listData: [],
    listName: ""
  };
  handleChange = event => {
    this.setState({
      listName: event.target.value
    });
  };
  componentDidMount() {
    const id = this.props.match.params.id;
    console.log(id)
    this.props.fetchLists(id);
  }

  render() {
    return (
      <div className="lists">
        {this.props.listData.map(c => (
          <Lists key={c.id} name={c.name} listdata={c} />
        ))}
        <div className="addlist-input-button">
          <input
            type="text"
            onChange={this.handleChange}
            value={this.state.listName}
          />
          <button onClick={this.addList} className="add-lists-button">
            Add Lists
          </button>
        </div>
      </div>
    );
  }
  addList = () => {
    const list = {
      boardId: this.props.match.params.id,
      listName: this.state.listName
    };
    this.props.postLists(list);
  };
}

const mapStateToProps = state => ({
  listData: state.boards.listData
});

export default connect(mapStateToProps, { fetchLists, postLists })(TotalLists);
