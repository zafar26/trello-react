// For fetching number of boards
// fetch(
//   "https://api.trello.com/1/members/mohammedzafar3/boards?filter=all&fields=all&lists=none&memberships=none&organization=false&organization_fields=name%2CdisplayName&key=a2688d977a464b393086096e6212869a&token=3889d13fe295eb3e43f4de58dc9cc6c6fba49d3f2c9efb05801519777932867f"
// )

// FOr Fetchiung numberof lists
// fetch(
//   `https://api.trello.com/1/boards/${id}/lists?key=${APIKEY}&token=${TOKEN}`
// )

// For Fetching number of Cards
// fetch( `https://api.trello.com/1/lists/${this.props.listID}/cards?key=${APIKEY}&token=${TOKEN}`)

//   .then(response => response.json())
//   .catch(err => console.log(err));

//   // For fetching lists
//   fetch("")
import React, { Component } from "react";
import Board from "./board";
import Header from "./header";
import onLoadURL from "../url";

class Index extends React.Component {
  state = {
    data: []
  };

  componentDidMount() {
    this.getAllCards();
  }
  render() {
    return (
      <div>
        <Header />
        <Board data={this.state.data} />
      </div>
    );
  }

  getAllCards = () => {
    fetch(onLoadURL)
      .then(response => response.json())
      .then(data => {
        this.setState({
          data: data
        });
        console.log(this.state.data);
      })
      .catch(error => {
        console.error("Error:", error);
      });
  };
}

export default Index;
