import React from "react";
import "./App.css";
import Header from "./header";
import { BrowserRouter as Router, Route } from "react-router-dom";

class App extends React.Component {
  // state = {};
  render() {
    return (
      <Router>
        <Route path="/" component={Header} />
      </Router>
    );
  }
}

export default App;
